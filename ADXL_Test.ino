#include "SimpleADLX345.h"

SimpleADLX345 accelerometer(true);
void setup() {
    // put your setup code here, to run once:
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    Serial.begin(38400);
    accelerometer.begin();
    digitalWrite(LED_BUILTIN, HIGH);
}

void loop() {
    // put your main code here, to run repeatedly:
    int16_t readings[3] = {0};
    accelerometer.read(readings);

    double reading = 0.049 * sqrt(pow(1.0*readings[0], 2) + pow(1.0*readings[1], 2) + pow(1.0*readings[2], 2));

    Serial.print(0.049 * readings[0]);
    Serial.print(",");
    Serial.print(0.049 * readings[1]);
    Serial.print(",");
    Serial.print(0.049 * readings[2]);
    Serial.print(",");
    Serial.println(reading);
    delay(10);
}
