#ifndef HALLTEST_SIMPLEADXL345_H
#define HALLTEST_SIMPLEADXL345_H

#include "Wire.h"
#define TheWire Wire

class SimpleADLX345 {
public:
    SimpleADLX345(bool sdo = true);
    ~SimpleADLX345() {}
    void begin();
    void read(int16_t *);
    void set(uint8_t reg, uint8_t value);
    // caller should have sizeof(uint16_t)*3 reserved for this
private:
    void mySend(uint8_t byte);
    uint8_t mAddress;
};

#endif //HALLTEST_SIMPLEADXL345_H
