#include "SimpleADLX345.h"

SimpleADLX345::SimpleADLX345(bool sdo) {
    // there is a pin labelled SDO that changes the I2C address between 0x3A (high) and 0x1D (low)
    mAddress = (sdo) ? 0x1D : 0x53;
}

void SimpleADLX345::begin() {
    Wire.begin();
    set(0x2D, 0x0);
    set(0x2C, 0x0F);
    set(0x38, 0x0);
    set(0x2D, 0x08);
    return;
}

void SimpleADLX345::mySend(uint8_t byte){
    Wire.write(byte);
}

void SimpleADLX345::read(int16_t *store) {
    Wire.beginTransmission(mAddress);
    mySend(0x32); // DATAX
    Wire.endTransmission();
    Wire.requestFrom(mAddress, (uint8_t)6);
    uint8_t *data = (uint8_t *)store;
    for (int i=0; i < 6; i++) {
        data[i] = Wire.read();
    }
    return;
}

void SimpleADLX345::set(uint8_t reg, uint8_t value) {
    Wire.beginTransmission(mAddress);
    mySend(reg);
    mySend(value);
    Wire.endTransmission();
}
